<?php

namespace App\Http\Controllers;

use App\Event;
use Illuminate\Http\Request;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        /*$validator = Validator::make(request()->all(), [
            'name' => 'required|string|unique:degrees',
            'department_id'=>'required|integer',
        ]);

        if ($validator->fails()){
            $failedRules = $validator->failed();

            // throw if name is not set
            if (isset($failedRules['name']['Required'])){
                throw new ApiException('Degree Name is required', 'Degree Name is required', 400);
            }elseif (isset($failedRules['name']['Unique'])){ // throw if email is not valid
                $name = \request()->get('name');
                throw new ApiException('Degree creation failed because the faculty name: '. $name .' already exists.' ,
                    'A Degree with name: '. $name .' already exists.',409);
            }
        }*/
       // $User=Department::find(request()->get('department_id'));

      /*  if ($department == null){
            throw new ApiException('Degree creation failed because the department id: '. $request->get('department_id')
                .' does not belongs to any department.' ,
                'A department with name: '. $request->get('department_id') .' does not belongs to any department',400);
        }*/

      /*   "name": "Tea Party 2018",
        "location": "Sabaragamuwa University",
        "description": "Event description in here",
        "start_at": "2018-09-19T19:30:00"*/

        $event = new Event();
        $event->name = \request()->get('name');
        $event->location=request()->get('location');
        $event->description=request()->get('description');
        $event->start_at=request()->get('start_at');

        return response()->json([
            'self' => request()->getSchemeAndHttpHost() .'/api/degrees/' . $event->id,
            'name' => $event->name,
            'location'=>$event->location,
            'description'=>$event->description,
            'start_at'=>$event->start_at,
          //  'created_by'=>request()->getSchemeAndHttpHost() . '/api/users/' .$user->id
        ], 201);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Event $event)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event)
    {
        //
    }
}
