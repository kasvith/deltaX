<?php

namespace App\Http\Controllers;

use App\Degree;
use App\Department;
use App\Exceptions\ApiException;
use App\Faculty;
use App\Mail\UserCreated;
use Dialog\Ideamart\SMS\SmsSender;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use App\User;
use Mockery\Exception;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all(['first_name', 'last_name', 'role', 'email', 'id', 'mobile']);

        foreach ($users as $user){
            $user->self = \request()->getSchemeAndHttpHost() . '/api/users/'. $user->id;
        }

        return response()->json($users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(request()->all(), [
            'email' => 'required|email|unique:users',
            'role' => 'nullable|in:user,admin,moderator',
            'first_name' => 'nullable|string',
            'last_name' => 'nullable|string',
            'mobile' => 'nullable|mobile',
            'faculty_id' => 'nullable',
            'degree_id' => 'nullable',
            'batch' => 'nullable',
            'password' => array('nullable','string','pmin', 'max:8')
        ]);

        if ($validator->fails()){
            $failedRules = $validator->failed();
            logger($failedRules);
            // throw if email is not set
            if (isset($failedRules['email']['Required'])){
                throw new ApiException('Email is required', 'Email is required', 400);
            }elseif (isset($failedRules['email']['Unique'])){ // throw if email is not valid
                $email = \request()->get('email');
                throw new ApiException('User creation failed because the email: '. $email .' already exists.',
                    'A user with email: '. $email .' already exists.', 409);
            }elseif (isset($failedRules['password'])){
                return response("", 400);
            }elseif(isset($failedRules['role']['In'])){
                $role = \request()->get('role');
                throw new ApiException('Cannot assign unknown role: '. $role .' to user',
                    'Unknown role is given', 400);
            }elseif(isset($failedRules['mobile']['Mobile'])){
                throw new ApiException('Malformed mobile number', 'Mobile number is not valid', 400);
            }
        }

        $password = \request()->get('password', null);

        if ($password == null){
            $password = $this->generateRandomString(6);
        }

        $user = new User();
        $user->first_name = \request()->get('first_name', '');
        $user->last_name = \request()->get('last_name', '');
        $user->password = bcrypt($password);
        $user->role = \request()->get('role', 'user');
        $user->email = \request()->get('email');
        $user->mobile = \request()->get('mobile');
        $user->batch = $request->get('batch');

        $fac = "";
        if ($request->get('faculty_id')){
            $faculty = Faculty::find($request->get('faculty_id'));

            if ($faculty){
                $user->faculty()->associate($faculty);
                $fac = [
                    'name' => $faculty->name,
                    'self' => $request->getSchemeAndHttpHost() . '/api/faculties/' . $faculty->id
                ];
            }
        }

        $depart = "";
        if ($request->get('department_id')){
            $dep = Department::find($request->get('department_id'));

            if ($dep){
                $user->department()->associate($dep);
                $depart = [
                    'name' => $dep->name,
                    'self' => $request->getSchemeAndHttpHost() . '/api/departments/' . $dep->id
                ];
            }
        }

        $degree = "";
        if ($request->get('degree_id')){
            $deg = Degree::find($request->get('degree_id'));

            if ($deg){
                $user->degree()->associate($deg);
                $degree =  [
                    'name' => $deg->name,
                    'self' => $request->getSchemeAndHttpHost() . '/api/degrees/' . $deg->id
                ];
            }
        }

        $user->save();


        if (\request()->get('mobile') == null)
            $user->mobile = "";

        if (\request()->get('batch') == null)
            $user->batch = "";

        if ($user->mobile != null){
            $this->sendSMS($user->mobile, $password);
        }elseif($user->email != null){
            $this->sendEmail($user->email, $password);
        }

        return response()->json([
            'self' => request()->getSchemeAndHttpHost() .'/api/users/' . $user->id,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'email' => $user->email,
            'role' => $user->role,
            'mobile' => $user->mobile,
            'batch' => $user->batch,
            'faculty' => $fac,
            'department' => $depart,
            'degree' => $degree
        ], 201);
    }

    private function sendSMS($mobile, $password){
        try{
            $sender = new SmsSender(config('app.dialog_api').'sms/send');
            $msg = "Your password is " . $password;
            $res = $sender->sms($msg, $mobile, config('app.dialog_password'), config('app.dialog_app'), config('app.dialog_source'),
             "0", "1", "0", "1.0", "");

            logger($res);
        }catch (\Exception $e){
            logger($e->getMessage());
        }
    }

    private function sendEmail($email, $pass){
        Mail::to($email)->send(new UserCreated($pass));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        if (!$user){
            throw new ApiException('User not exists', 'User not exists', 404);
        }

        $fac = null;
        if ($user->faculty != null){
            $fac = [
                'name' => $user->faculty->name,
                'self' => \request()->getSchemeAndHttpHost() . '/api/faculties/' . $user->faculty->id
            ];
        }

        $depart = null;
        if ($user->departmant != null){
            $fac = [
                'name' => $user->faculty->name,
                'self' => \request()->getSchemeAndHttpHost() . '/api/departments/' . $user->departmant->id
            ];
        }

        $degree = null;
        if ($user->degree != null){
            $fac = [
                'name' => $user->faculty->name,
                'self' => \request()->getSchemeAndHttpHost() . '/api/degrees/' . $user->degree->id
            ];
        }

        if ($user->mobile == null){
            $user->mobile = '';
        }

        if ($user->batch == null){
            $user->batch = '';
        }

        if ($user->first_name == null){
            $user->first_name = '';
        }

        if ($user->last_name == null){
            $user->last_name = '';
        }

        return response()->json([
            'self' => request()->getSchemeAndHttpHost() .'/api/users/' . $user->id,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'email' => $user->email,
            'role' => $user->role,
            'batch' => $user->batch,
            'faculty' => $fac,
            'department' => $depart,
            'degree' => $degree
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function generateRandomString($length = 8) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 5; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        $upper = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $lower = 'abcdefghijklmnopqrstuvwxyz';
        $special = "~!@#$%^&*_-+=`|(){}[].?";
        $randomString .= $upper[rand(0, strlen($upper))];
        $randomString .= $lower[rand(0, strlen($lower))];
        $randomString .= $special[rand(0, strlen($special))];

        return $randomString;
    }
}
