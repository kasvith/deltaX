<?php

namespace App\Http\Controllers;

use App\Degree;
use App\Department;
use Illuminate\Http\Request;
use App\Exceptions\ApiException;
use Illuminate\Support\Facades\Validator;

class DegreeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (\request()->input('department')){
            $department = Department::find(\request()->input('department'));
            if ($department == null){
                throw new ApiException('Department does not exists' ,
                    'Department does not exists',409);
            }

            $degrees= $department->degrees;
        }else{
            $degrees=Degree::all(['id','name']);
        }



        foreach ($degrees as $degree)
        {
            $degree-> self = request()->getSchemeAndHttpHost() . '/api/degrees/' . $degree->id;
        }

        return response()->json(["degrees" => $degrees]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        /* "name": "B.Sc. Computer Science",
    "department_id": "{department_id}
        */
        $validator = Validator::make(request()->all(), [
            'name' => 'required|string|unique:degrees',
            'department_id'=>'required|integer',
        ]);

        if ($validator->fails()){
            $failedRules = $validator->failed();

            // throw if name is not set
            if (isset($failedRules['name']['Required'])){
                throw new ApiException('Degree Name is required', 'Degree Name is required', 400);
            }elseif (isset($failedRules['name']['Unique'])){ // throw if email is not valid
                $name = \request()->get('name');
                throw new ApiException('Degree creation failed because the faculty name: '. $name .' already exists.' ,
                    'A Degree with name: '. $name .' already exists.',409);
            }
        }
        $department=Department::find(request()->get('department_id'));

        if ($department == null){
            throw new ApiException('Degree creation failed because the department id: '. $request->get('department_id')
                .' does not belongs to any department.' ,
                'A department with name: '. $request->get('department_id') .' does not belongs to any department',400);
        }


        $degree = new Degree();
        $degree->name = \request()->get('name');
        //$degree->department_id=request()->get('department_id');    $department->faculty()->associate($faculty);
        $degree->department()->associate($department);
        $degree->save();

        return response()->json([
            'self' => request()->getSchemeAndHttpHost() .'/api/degrees/' . $degree->id,
            'name' => $degree->name,
            'department' => [
                'self' => request()->getSchemeAndHttpHost() .'/api/departments/' .$department->id ,
                'name' => $department->name,

            ]
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Degree  $degree
     * @return \Illuminate\Http\Response
     */
    public function show(Degree $degree)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Degree  $degree
     * @return \Illuminate\Http\Response
     */
    public function edit(Degree $degree)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Degree  $degree
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Degree $degree)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Degree  $degree
     * @return \Illuminate\Http\Response
     */
    public function destroy(Degree $degree)
    {
        //
    }
}
