<?php

namespace App\Http\Controllers;

use App\Faculty;
use Illuminate\Http\Request;
use App\Exceptions\ApiException;
use Illuminate\Support\Facades\Validator;

class FacultyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $faculties=Faculty::all(['id','name']);

        foreach ($faculties as $faculty)
        {
            $faculty-> self = request()->getSchemeAndHttpHost() . '/api/faculties/' . $faculty->id;
        }

        return response()->json(["faculties" => $faculties]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $validator = Validator::make(request()->all(), [
            'name' => 'required|string|unique:faculties'
        ]);

        if ($validator->fails()){
            $failedRules = $validator->failed();

            // throw if email is not set
            if (isset($failedRules['name']['Required'])){
                throw new ApiException('Name is required', 'Name is required', 400);
            }elseif (isset($failedRules['name']['Unique'])){ // throw if email is not valid
                $name = \request()->get('name');
                throw new ApiException('Faculty creation failed because the faculty name: '. $name .' already exists.' ,
                    'A faculty with name: '. $name .' already exists.',409);
            }
        }

        $faculty = new Faculty();
        $faculty->name = \request()->get('name');
        $faculty->save();

        return response()->json([
            'self' => request()->getSchemeAndHttpHost() .'/api/faculties/' . $faculty->id,
            'name' => $faculty->name
        ], 201);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Faculty  $faculty
     * @return \Illuminate\Http\Response
     */
    public function show(Faculty $faculty)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Faculty  $faculty
     * @return \Illuminate\Http\Response
     */
    public function edit(Faculty $faculty)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Faculty  $faculty
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Faculty $faculty)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Faculty  $faculty
     * @return \Illuminate\Http\Response
     */
    public function destroy(Faculty $faculty)
    {
        //
    }
}
