<?php
namespace App\Http\Controllers;
use App\Department;
use App\Exceptions\ApiException;
use App\Faculty;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (\request()->input('faculty')){
            $faculty = Faculty::find(\request()->input('faculty'));
            if ($faculty == null){
                throw new ApiException('Faculty does not exists' ,
                    'Faculty does not exists',409);
            }

            $departments = $faculty->departments;
        }else{
            $departments = Department::all();
        }

        foreach ($departments as $department){
            $department->self = \request()->getSchemeAndHttpHost() . '/api/departments/' . $department->id;
        }
        return response()->json(["departments" => $departments]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(request()->all(), [
            'name' => 'required|string|unique:departments',
            'faculty_id' => 'required|integer',
        ]);
        if ($validator->fails()){
            $failedRules = $validator->failed();
            // throw if email is not set
            if (isset($failedRules['name']['Required'])){
                throw new ApiException('Department name is required', 'Name is required', 400);
            }elseif (isset($failedRules['name']['Unique'])){ // throw if email is not valid
                $name = \request()->get('name');
                throw new ApiException('Department creation failed because the faculty name: '. $name .' already exists.' ,
                    'A faculty with name: '. $name .' already exists.',409);
            }
        }
        $faculty = Faculty::find($request->get('faculty_id'));
        if ($faculty == null){
            throw new ApiException('Department creation failed because the faculty id: '. $request->get('faculty_id')
                .' does not belongs to any faculty.' ,
                'A faculty with name: '. $request->get('faculty_id') .' does not belongs to any faculty',400);
        }
        $department = new Department();
        $department->name = \request()->get('name');
        $department->faculty()->associate($faculty);
        $department->save();
        return response()->json([
            'self' => request()->getSchemeAndHttpHost() .'/api/departments/' . $department->id,
            'name' => $department->name,
            'faculty' => [
                'self' => request()->getSchemeAndHttpHost() .'/api/faculties/' . $faculty->id,
                'name' => $faculty->name,
            ]
        ], 201);
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function show(Department $department)
    {
        //
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function edit(Department $department)
    {
        //
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Department $department)
    {
        //
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function destroy(Department $department)
    {
        //
    }
}