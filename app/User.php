<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'role', 'mobile', 'batch'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'id' => 'string',
        'mobile' => 'string'
    ];

    public function department(){
        return $this->belongsTo(Department::class);
    }

    public function faculty(){
        return $this->belongsTo(Faculty::class);
    }

    public function degree(){
        return $this->belongsTo(Degree::class);
    }
}
