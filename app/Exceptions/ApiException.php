<?php

namespace App\Exceptions;

use Exception;


class ApiException extends Exception
{
    protected $developerMessage;

    public function __construct($devMessage ="", $message = "", $code = 0, $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->developerMessage = $devMessage;
    }

    /**
     * @return string
     */
    public function getDeveloperMessage()
    {
        return $this->developerMessage;
    }
}
