<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Degree extends Model
{
    //
    protected $guarded=[];

    protected $casts = [
        'id' => 'string'
    ];

    protected $fillable = ['name','department_id'];

    public function department(){
        return $this->belongsTo(Department::class);
    }

    public function users(){
        return $this->hasMany(User::class);
    }
}
