<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Faculty extends Model
{
    protected $fillable = ['name'];

    public function departments(){
        return $this->hasMany(Department::class)->select(['id', 'name']);
    }

    public function users(){
        return $this->hasMany(User::class);
    }
}
