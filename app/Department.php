<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $fillable = ['name'];

    protected $casts = [
        'id' => 'string'
    ];

    public function faculty(){
        return $this->belongsTo(Faculty::class);
    }

    public function degrees(){
        return $this->hasMany(Degree::class);
    }

    public function users(){
        return $this->hasMany(User::class);
    }
}
