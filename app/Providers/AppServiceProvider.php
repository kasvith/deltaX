<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        Validator::extend('mobile', function ($attribute, $value, $parameters){
            return strlen($value) == 12 && substr($value, 0, 3) == '+94' && is_numeric(substr($value, -11 )) == true;
        });

        Validator::extend('password', function ($attribute, $value, $parameters){
            return preg_match('/^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}$/', $value);
        });

        Validator::extend('pmin', function ($attribute, $value, $parameters){
            return !(strlen($value) < 6);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
