<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/', function (){
    return response('', 200);
});


// Users routes
Route::group(['prefix' => 'users'], function (){
    Route::post('/', 'UserController@store'); // creates a user
    Route::get('/', 'UserController@index');
    Route::get('/{id}', 'UserController@show');
});

// Faculties routes
Route::group(['prefix' => 'faculties'], function (){
    Route::get('/', 'FacultyController@index');
    Route::post('/', 'FacultyController@store');
});

// Departments routes
Route::group(['prefix' => 'departments'], function (){
    Route::get('/', 'DepartmentController@index');
    Route::post('/', 'DepartmentController@store');
});

//Degree routes
Route::group(['prefix' => 'degrees'], function () {
    Route::get('/', 'DegreeController@index');
    Route::post('/', 'DegreeController@store');
});

//Event routes
Route::group(['prefix' => 'events'], function () {
    Route::get('/', 'EventController@index');
    Route::post('/', 'EventController@store');
});