<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
   /** public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->String('name');
            $table->String("location");
            $table->String("description");
            $table->date("start_at");
            $table->integer('created_by');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
   /** public function down()
    {
        Schema::dropIfExists('events');
    }*/
}
